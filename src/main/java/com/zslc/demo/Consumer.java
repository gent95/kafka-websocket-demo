package com.zslc.demo;

import cn.hutool.setting.dialect.Props;
import com.alibaba.fastjson.JSONObject;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.websocket.Session;
import java.io.IOException;
import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * @author: create by majt
 * @version: v1.0
 * @description: com.zslc.demo
 * @date:2019/7/12 17:37
 */
public class Consumer implements Runnable {
    private final Logger log = LoggerFactory.getLogger(Consumer.class);
    private Session session;

    public Consumer(Session session) {
        this.session = session;
    }

    @Override
    public void run() {
        Props prop = new Props("application.properties--");
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, prop.getStr("bootstrap-servers"));
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "test_groupId");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        Set<String> topics = getTopics(Constans.RULE_MAP.get(session.getId()));
        if (null == topics || topics.size() == 0) {
            try {
                session.getBasicRemote().sendText("请保证tids属性和topics属性至少一个有数据");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(props);
        kafkaConsumer.subscribe(topics);
        while (true) {
            ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ZERO);
            records.forEach(record -> {
                log.info("监听到kafka的消息 {}", record.value());
                try {
                    JSONObject jsonObject = JSONObject.parseObject(record.value());
                    String message = getMessage(jsonObject, Constans.RULE_MAP.get(session.getId()));
                    if (null != message) {
                        session.getBasicRemote().sendText(message);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    /**
     * 获取客户端要监听的主题
     *
     * @param jsonMsg
     * @return
     */
    private Set<String> getTopics(JSONObject jsonMsg) {
        if (null == jsonMsg.get(Constans.TIDS) && null == jsonMsg.get(Constans.TOPICS)) {
            return null;
        }
        Set<String> topics = new HashSet<>();
        List<String> tids = (List<String>) jsonMsg.get(Constans.TIDS);
        List<String> myTopics = (List<String>) jsonMsg.get(Constans.TOPICS);
        if (null != tids) {
            tids.stream().forEach(tid -> {
                topics.add("data" + tid);
            });
        }

        if (null != myTopics) {
            myTopics.stream().forEach(myTopic -> {
                topics.add(myTopic);
            });
        }
        return topics;
    }

    /**
     * 根据客户端发送的规则过滤要发送到客户端的消息
     *
     * @param jsonObject
     * @param ruleObject
     * @return
     */
    private String getMessage(JSONObject jsonObject, JSONObject ruleObject) {
        String msgType = jsonObject.get("msg").toString();
        List<String> types = (List<String>) ruleObject.get("types");
//        if (null == jsonObject.get(Constans.SRC_SYS) && null == jsonObject.get(Constans.SRC_SYS)){
//            if (ruleObject.get(Constans.SID).equals(jsonObject.get(Constans.SID).toString())) {
//                if (null != types && types.contains(msgType)) {
//                    return jsonObject.toJSONString();
////                }
////            }
//        }

        if (ruleObject.get(Constans.SRC_SYS).equals(jsonObject.get(Constans.SRC_SYS))) {
            if (ruleObject.get(Constans.DEST_SYS).equals(jsonObject.get(Constans.DEST_SYS))) {
                if (ruleObject.get(Constans.SID).equals(jsonObject.get(Constans.SID).toString())) {
                    if (null != types && types.contains(msgType)) {
                        return jsonObject.toJSONString();
                    }
                }
            }
        }
        return null;
    }
}
