package com.zslc.demo;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author: create by majt
 * @version: v1.0
 * @description: com.zslc.demo
 * @date:2019/7/11 16:26
 */
@ServerEndpoint("/")
@Component
public class WebSocketServer {
    private final Logger log = LoggerFactory.getLogger(WebSocketServer.class);
    private static int onlineCount = 0;
    private static CopyOnWriteArraySet<WebSocketServer> webSocketSet = new CopyOnWriteArraySet<>();
    private Session session;

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
        Constans.SESSION_SET.add(session);
        webSocketSet.add(this);
        addOnlineCount();
        Constans.RULE_MAP.put(session.getId(), null);
        sendMessage("连接成功");
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        webSocketSet.remove(this);
        Constans.SESSION_SET.remove(session);
        Constans.RULE_MAP.remove(session.getId());
        subOnlineCount();
        log.info("连接关闭，删除规则");
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        log.info("收到客户端发来的消息 {}", message);
        if (StringUtils.isNotBlank(message)) {
            try{
                JSONObject jsonMsg = JSONObject.parseObject(message);
                Constans.RULE_MAP.replace(session.getId(), jsonMsg);
            }catch (JSONException e){
                sendMessage("筛选规则有误！"+message+" 请发送正确消息格式如：\n{\"src_sys\":\"Q\",\"dest_sys\":\"L\",\"sid\":\"1\",\"tids\":[\"1\"],\"types\":[\"D\"]}");
            }
        }
    }

    /**
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    /**
     * 实现服务器主动推送
     */
    public void sendMessage(String message) {
        if (null != this.session) {
            try {
                this.session.getBasicRemote().sendText(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 群发自定义消息
     */
    public void sendInfo(String message) throws IOException {
        for (WebSocketServer item : webSocketSet) {
            item.sendMessage(message);
        }
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        WebSocketServer.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketServer.onlineCount--;
    }
}


