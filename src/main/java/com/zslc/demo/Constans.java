package com.zslc.demo;

import cn.hutool.core.thread.ThreadUtil;
import com.alibaba.fastjson.JSONObject;

import javax.websocket.Session;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author: create by majt
 * @version: v1.0
 * @description: 系统常量
 * @date:2019/7/12 15:37
 */
public interface Constans {
    HashMap<String, JSONObject> RULE_MAP = new HashMap<>();
    ThreadPoolExecutor CONSUMER_POOL = ThreadUtil.newExecutor(5,50);
//    CopyOnWriteArraySet<WebSocketServer> WEB_SOCKET_SET = new CopyOnWriteArraySet<>();
    Set<Session> SESSION_SET = new HashSet<>();
    /**
     * 客户端参数定义
     */
    String TIDS = "tids";
    /**
     * 来源系统
     */
    String SRC_SYS = "src_sys";
    /**
     * 目标系统
     */
    String DEST_SYS = "dest_sys";
    /**
     * 已知的kafka主题
     */
    String TOPICS = "topics";

    /**
     * 自动调度系统
     */
    String MANAGEMENT = "M";
    /**
     * 逻辑判断系统
     */
    String LOGIC = "L";
    /**
     * 质量检测系统
     */
    String QUALITY = "Q";

    /**
     * 报文标识规范
     * <p>
     * 数据
     */
    String DATA = "D";
    /**
     * 告警
     */
    String ALARM = "A";
    /**
     * 指令
     */
    String COMMAND = "C";
    /**
     * 描述
     */
    String INFO = "I";
    /**
     * 心跳
     */
    String HEART = "H";

    String SID = "sid";
}
