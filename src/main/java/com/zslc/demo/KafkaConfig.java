package com.zslc.demo;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.ConsumerFactory;

/**
 * @author: create by majt
 * @version: v1.0
 * @description: com.zslc.demo
 * @date:2019/7/18 13:06
 */
@Configuration
public class KafkaConfig implements InitializingBean {
    @Autowired
    private ConsumerFactory<String, String> consumerFactory;

    @Value("${local_alarm}")
    private String localAlarm;

    @Value("${count}")
    private int count;

    public String getTopicsStr() {

        String topicsStr="";

        for (int i = 1; i <count ; i++) {
            topicsStr+="data"+i+",";
        }
        topicsStr+=localAlarm;
        return topicsStr;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String logicTopicName = getTopicsStr();
        System.setProperty("DataTopic", logicTopicName);
    }
}
