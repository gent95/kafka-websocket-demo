package com.zslc.demo;

import com.alibaba.fastjson.JSONObject;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import javax.websocket.Session;
import java.util.List;

/**
 * @author: create by majt
 * @version: v1.0
 * @description: kafka管理类
 * @date:2019/7/16 14:49
 */
@Component
public class KafkaManager {
    private final Logger log = LoggerFactory.getLogger(KafkaManager.class);

    @KafkaListener(id = "device-data", topics = "#{'${DataTopic}'.split(',')}")
    public void proce(ConsumerRecord<String, String> record) {
        String recordValue = record.value();
        log.info("监听到kafka新消息 {}", recordValue);
        Constans.SESSION_SET.forEach(session -> {
            JSONObject rule = Constans.RULE_MAP.get(session.getId());
            if (null == rule) {
                sendMessage(session, recordValue);
            } else {
                String message = getMessage(JSONObject.parseObject(recordValue), rule);
                sendMessage(session, message);
            }
        });
    }

    private String getMessage(JSONObject jsonObject, JSONObject ruleObject) {
        String msgType = null == jsonObject.get("msg") ? null : jsonObject.get("msg").toString();
        String tid = null == jsonObject.get("tid") ? null : jsonObject.get("tid").toString();
        List<String> types = (List<String>) ruleObject.get("types");
        List<String> tids = (List<String>) ruleObject.get("tids");

        if (null != ruleObject.get(Constans.SRC_SYS)) {
            if (!jsonObject.get(Constans.SRC_SYS).equals(ruleObject.get(Constans.SRC_SYS))) {
                return null;
            }
        }

        if (null != ruleObject.get(Constans.DEST_SYS)) {
            if (!jsonObject.get(Constans.DEST_SYS).equals(ruleObject.get(Constans.DEST_SYS))) {
                return null;
            }
        }

        if (null != ruleObject.get(Constans.SID)) {
            if (!jsonObject.get(Constans.SID).toString().equals(ruleObject.get(Constans.SID).toString())) {
                return null;
            }
        }

        if (null != tids) {
            if (!tids.contains(tid)) {
                return null;
            }
        }

        if (null != types) {
            if (!types.contains(msgType)) {
                return null;
            }
        }
        return jsonObject.toJSONString();
    }

    /**
     * 发送消息给指定的会话
     *
     * @param session
     * @param message
     */
    private void sendMessage(Session session, String message) {
        try {
            session.getBasicRemote().sendText(message);
        } catch (Exception e) {

        }
    }
}
