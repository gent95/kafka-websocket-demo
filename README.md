# kafka-websocket 调试工具
## 参数说明 (以下字段为必选字段不可缺省)
- src_sys: 数据来源(String)  可选值：Q， M
- dest_sys: 目标系统(String) 可选值：L
- sid: 地球站地址(String)
- tids: 业务id（Array）       可选值：1-50
- types: 消息类型(Array)      可选值:A ,D, C, I

### 例：
```json
{
    "src_sys":"Q",
    "dest_sys":"L",
    "sid":"1",
    "tids":["1","2"],
    "types":["A","D"]
}
```
